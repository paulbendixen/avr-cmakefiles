macro( target_makeHex tgt )
	add_custom_command( OUTPUT ${tgt}.hex
		DEPENDS ${tgt}
		COMMAND ${CMAKE_OBJCOPY} -R .eeprom -O ihex ${tgt} ${tgt}.hex
		COMMENT "Generating hex file for ${tgt} using ${CMAKE_OBJCOPY}"
		)
		set( AVR_DUDE_PROCESSOR "")
		string( REPLACE atmega m AVR_DUDE_PROCESSOR ${AVR_PROCESSOR} )
	add_custom_target( program_${tgt} DEPENDS ${tgt}.hex
		COMMAND ${AVR_PROGRAMMER} -c atmelice_isp -p ${AVR_DUDE_PROCESSOR} -e -u -U ${tgt}.hex -B 10 )
endmacro()