# Toolchainfile for the AVR

set( CMAKE_SYSTEM_NAME Generic )

set( CMAKE_C_COMPILER avr-gcc )
set( CMAKE_CXX_COMPILER avr-g++ )
set( CMAKE_OBJCOPY avr-objcopy)

set( CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY )

set( CMAKE_PREFIX_PATH /usr/lib/avr )

if ( AVR_PROCESSOR )
	set( ENV{_AVR_PROCESSOR} "${AVR_PROCESSOR}" )
else()
	if( NOT $ENV{_AVR_PROCESSOR} STREQUAL "" )
		set( AVR_PROCESSOR $ENV{_AVR_PROCESSOR} )
	else()
		message( "Processor not set (set with -D AVR_PROCESSOR=xxx)." )
		message( "Setting to atmega328p" )
		set( AVR_PROCESSOR atmega328p )
		set( ENV{_AVR_PROCESSOR} "${AVR_PROCESSOR}" )
	endif()
endif()

set( CMAKE_C_FLAGS_INIT -mmcu=${AVR_PROCESSOR} CACHE STRING "" FORCE )
set( CMAKE_CXX_FLAGS_INIT "-mmcu=${AVR_PROCESSOR}" CACHE STRING "" FORCE )
set( CMAKE_EXE_LINKER_FLAGS_INIT "-mmcu=${AVR_PROCESSOR} -Wl,--gc-sections -mrelax" CACHE STRING "" FORCE )
message( "Setting the C++ flags to ${CMAKE_CXX_FLAGS_INIT}" )
set( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER )
set( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )

find_program( AVR_PROGRAMMER avrdude )
