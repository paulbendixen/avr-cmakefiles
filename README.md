CMake files for using the AVR processor

The purpose of these files are to have a as modern as possible cmake file for
your AVR project.

Using the project
=================

add `avr-cmakefiles` as a submodule (or if you are religeous about it some thing
else) and add the toolchain file using
`-DCMAKE_TOOLCHAIN_FILE=AVR-toolchain.cmake`.

To use avrdude (or whatever you change it to) include the `AVR-Utilities.cmake`
and use `target_makeHex( ${TARGET} )`